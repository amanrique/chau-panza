<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\FreeRegisterRequest;
use ChauPanza\User\UserRepository;
use MP;

class MercadoPagoController extends Controller
{
	private $userRepo;
	public function __construct( UserRepository $userRepo)
    {
    	$this->userRepo = $userRepo;
      
    }

	public function urlUserPlan(Request $request){
		$user = $this->getUserByToken($request);
		$plan = \ChauPanza\Plan\Plan::find($request->plan_id);
		if(!$plan){
			return json_encode(['status' => 'error', 'message' => 'Plan invalido']);
		}else{
			$url = 	"http://google.com/".$request->plan_id;

			$preferenceData = [
	            'items'              => [
	                [
	                    'id'          => '123',
	                    'title'       => 'ChauPanza: Suscripción plan '.$plan->name.' '.$user->email,
	                    'description' => 'Description',
	                    //'picture_url' => 'https://chaupanza.mensorestudio.com.ar/img/logo2.png',
	                    'quantity'    => 1,
	                    'currency_id' => 'ARS',
	                    'unit_price'  => $plan->price,
	                ],
	            ],
	            'back_urls'          => [
	                'success' => 'https://aatalac.org.ar/cuota-curso/aprobado',
	                'pending' => 'https://aatalac.org.ar/cuota-curso/pendiente',
	                'failure' => 'https://aatalac.org.ar/cuota-curso/fallo',
	            ],
	            'external_reference' => 'external reference',
	            'notification_url'   => 'https://aatalac.org.ar/cuota-curso/ipn',
	            'reason'             => 'reason',
	        ];
	        $preference                 = MP::create_preference($preferenceData);

			return  json_encode(['status' => 'ok', 'url' => $preference['response']['init_point']]); 
		
		}
	
	}
}